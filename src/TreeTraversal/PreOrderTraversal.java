package TreeTraversal;

import java.util.ArrayList;
import java.util.List;

public class PreOrderTraversal implements Traversal {


	public List<Node> traverse(Node node){
		List<Node> tree = new ArrayList<Node>();
		if(node==null){
			return new ArrayList<Node>();
		}
		else{
			tree.add(node);
			tree.addAll(traverse(node.getLeft()));
			tree.addAll(traverse(node.getRight()));
			return tree;		
		}
	}
}

 