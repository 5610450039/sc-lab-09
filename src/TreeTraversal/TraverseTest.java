package TreeTraversal;



import java.util.ArrayList;

public class TraverseTest {


	public static void main(String[] args){

		Node a = new Node("A",null,null);
		Node c = new Node("C",null,null);
		Node e = new Node("E",null,null);
		Node d = new Node("D",c,e);
		Node b = new Node("B",a,d);
		Node h = new Node("H",null,null);
		Node i = new Node("I",h,null);
		Node g = new Node("G",null,i);
		Node f = new Node("F",b,g);

		ReportConsole rpc = new ReportConsole();
		
		rpc.display(f, new PreOrderTraversal());
		rpc.display(f, new InOrderTraversal());
		rpc.display(f, new PostOrderTraversal());
	}
	

}
