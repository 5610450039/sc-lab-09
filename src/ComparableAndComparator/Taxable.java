package ComparableAndComparator;


public interface Taxable {
	
	double getTax();
	String getName();

}
