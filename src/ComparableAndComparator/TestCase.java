package ComparableAndComparator;
import java.util.ArrayList;
import java.util.Collections;


public class TestCase {
	public static void main(String[] args){
		System.out.println("---Person---");
		ArrayList<Person> personList = new ArrayList<Person>();
		personList.add(new Person("Black",75000));
		personList.add(new Person("Keen",100000));
		personList.add(new Person("Pond",50000));
		
		System.out.println("-before sort income in Person");
		for (Person ps : personList) 
			System.out.println(ps.getIncome());

		Collections.sort(personList);
		
		System.out.println("-after sort income in Person");
		for (Person ps : personList) 
			System.out.println(ps.getIncome());
		
		ArrayList<Product> productList = new ArrayList<Product>();
		productList.add(new Product("Pencil",150));
		productList.add(new Product("Pen",200));
		productList.add(new Product("Rubber",50));
		
		System.out.println("\n---Product---");
		System.out.println("-before sort price in Product");
		for (Product pd : productList) 
			System.out.println(pd.getPrice());

		Collections.sort(productList);
		
		System.out.println("-after sort price in Product");
		for (Product pd : productList) 
			System.out.println(pd.getPrice());
		
		 
		System.out.println("\n---Company---");
		ArrayList<Company> companyList = new ArrayList<Company>();
		companyList.add(new Company("Nokia", 500000, 400000));
		companyList.add(new Company("Sumsung", 300000, 100000));
		companyList.add(new Company("Apple", 400000, 50000));
		
		System.out.println("-Before sort");
		for (Company c : companyList) {
			System.out.println(c);
		}
				
		Collections.sort(companyList, new EarningComparator());
		System.out.println("\n-After sort with Earning");
		for (Company c : companyList) {
			System.out.println(c);
		}
		
		Collections.sort(companyList, new ExpenseComparator());
		System.out.println("\n-After sort with Expense");
		for (Company c : companyList) {
			System.out.println(c);
		}

		Collections.sort(companyList, new ProfitComparator());
		System.out.println("\n-After sort with Profit");
		for (Company c : companyList) {
			System.out.println(c);
		}
		
		System.out.println("\n---Taxable of Person, Product, Company---");
		ArrayList<Taxable> taxList = new ArrayList<Taxable>();
		taxList.add(new Person("Keen",1000000));
		taxList.add(new Product("Computer",100000));
		taxList.add(new Company("Apple",400000,50000));
		
		System.out.println("-Before sort");
		for (Taxable t : taxList) {
			System.out.println("Tax of "+t.getName()+" is "+t.getTax());
		}
		
		Collections.sort(taxList, new TaxComparator());
		System.out.println("\n-After sort with Taxable");
		for (Taxable t : taxList) {
			System.out.println("Tax of "+t.getName()+" is "+t.getTax());
		
		}
	}


}
	
